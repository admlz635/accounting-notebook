# AgileEngine: Accounting Notebook

Built with SpringBoot. This projects its a simple REST API based on https://agileengine.bitbucket.io/fsNDJmGOAwqCpzZx/api/#/.

Resources:
* Post    `/api/transactions` Commit new transaction to the account
* Get     `/api/transactions` Fetches transactions history
* Get     `/api/transactions/:id` Find transaction by ID

To start, run the command `./mvnw spring-boot:run`

React project is in 'accountingnbook' dir.
First run `npm install` to install all the required dependencies.

To start frontend, run `npm start`

To build a jar with frontend packaged , first run `npm run build` and then copy all the content of accountingnbook/build to
 src/main/resources/static. Finally run `./mvnw package`. The jar will be at target/demo-0.0.1-SNAPSHOT.jar
 
Bundled version is in 'dist/demo-0.0.1-SNAPSHOT.jar'. You can run it with the command `java -jar demo-0.0.1-SNAPSHOT.jar`
