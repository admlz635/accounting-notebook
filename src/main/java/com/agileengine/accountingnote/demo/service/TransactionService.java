package com.agileengine.accountingnote.demo.service;

import com.agileengine.accountingnote.demo.dto.TransactionDTO;
import com.agileengine.accountingnote.demo.dto.TransactionResponseDTO;
import com.agileengine.accountingnote.demo.dto.Type;
import com.agileengine.accountingnote.demo.exception.AccountingNotebookException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;

@Service
public class TransactionService {

    private final Logger log = LoggerFactory.getLogger(TransactionService.class);

    // Account value
    private BigDecimal value = new BigDecimal(0);

    // Storage
    private Map<String, TransactionResponseDTO> all = new HashMap<>();

    private final ReadWriteLock lock = new ReentrantReadWriteLock();

    public TransactionResponseDTO findById(String id) {

        log.debug("Request to get TransactionResponseDTO : {}", id);


        try {
            UUID.fromString(id);
        } catch (IllegalArgumentException e) {
            log.debug("Invalid ID", e);
            throw new AccountingNotebookException("invalid ID supplied");
        }
        lock.readLock().lock();
        try {
            return Optional.ofNullable(all.get(id)).orElseThrow(() -> new AccountingNotebookException("transaction not found"));
        } finally {
            lock.readLock().unlock();
        }

    }

    public List<TransactionResponseDTO> findAll() {
        log.debug("Request to get all TransactionResponseDTO");
        lock.readLock().lock();
        try {
            return all.values().stream().sorted(Comparator.comparing(TransactionResponseDTO::getEffectiveDate).reversed())
                    .collect(Collectors.toList());
        } finally {
            lock.readLock().unlock();
        }
    }

    private void doTransaction(BigDecimal amount, Type type) {
        // log.debug("amount {} - current account value {}", amount, value);

        if (Type.debit == type) {
            if (amount.compareTo(value) > 0) {
                throw new AccountingNotebookException("insufficient funds");
            }
            value = value.subtract(amount);
            return;
        }
        value = value.add(amount);
    }

    public TransactionResponseDTO save(TransactionDTO transactionDTO) {
        log.debug("Request to save TransactionDTO : {}", transactionDTO);
        TransactionResponseDTO out = new TransactionResponseDTO();

        lock.writeLock().lock();
        try {
            doTransaction(transactionDTO.getAmount(), transactionDTO.getType());
            out.setEffectiveDate(LocalDateTime.now().toString());
            out.setId(UUID.randomUUID().toString());
            out.setAmount(transactionDTO.getAmount());
            out.setType(transactionDTO.getType());
            all.put(out.getId(), out);
            return out;
        } finally {
            lock.writeLock().unlock();
        }

    }
}
