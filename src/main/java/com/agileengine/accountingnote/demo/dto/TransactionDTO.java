package com.agileengine.accountingnote.demo.dto;

import java.math.BigDecimal;

public class TransactionDTO {

    private Type type;

    private BigDecimal amount;


    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "TransactionDTO{" +
                "type=" + type +
                ", amount=" + amount +
                '}';
    }
}
