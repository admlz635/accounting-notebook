package com.agileengine.accountingnote.demo.dto;

public enum Type {
    credit, debit
}
