package com.agileengine.accountingnote.demo.dto;

import java.math.BigDecimal;

public class TransactionResponseDTO {

    private String id;

    private Type type;

    private BigDecimal amount;

    private String effectiveDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    @Override
    public String toString() {
        return "TransactionResponseDTO{" +
                "id='" + id + '\'' +
                ", type=" + type +
                ", amount=" + amount +
                ", effectiveDate='" + effectiveDate + '\'' +
                '}';
    }
}
