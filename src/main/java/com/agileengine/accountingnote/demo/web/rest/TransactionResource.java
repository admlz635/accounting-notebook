package com.agileengine.accountingnote.demo.web.rest;

import com.agileengine.accountingnote.demo.dto.ExceptionDTO;
import com.agileengine.accountingnote.demo.dto.TransactionDTO;
import com.agileengine.accountingnote.demo.dto.TransactionResponseDTO;
import com.agileengine.accountingnote.demo.exception.AccountingNotebookException;
import com.agileengine.accountingnote.demo.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * REST controller for managing transactions.
 */
@RestController
@RequestMapping("/api")
public class TransactionResource {

    private final Logger log = LoggerFactory.getLogger(TransactionResource.class);

    private final TransactionService transactionService;

    public TransactionResource(TransactionService transactionService) {
        this.transactionService = transactionService;
    }


    @PostMapping("/transactions")
    public ResponseEntity<TransactionResponseDTO> createTransaction(@RequestBody TransactionDTO transactionDTO) {

        TransactionResponseDTO response = transactionService.save(transactionDTO);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(response);
    }

    @GetMapping("/transactions/{id}")
    public ResponseEntity<TransactionResponseDTO> getTransaction(@PathVariable String id) {

        TransactionResponseDTO response = transactionService.findById(id);
        return ResponseEntity.status(HttpStatus.OK)
                .body(response);
    }

    @GetMapping("/transactions")
    public List<TransactionResponseDTO> getAllTransactions() {
        return transactionService.findAll();
    }

    @ExceptionHandler(AccountingNotebookException.class)
    @ResponseBody
    public ResponseEntity<ExceptionDTO> exceptionHandler(AccountingNotebookException e) {
        log.error("Error", e);
        return new ResponseEntity<>(new ExceptionDTO(e.getMessage()), HttpStatus.BAD_REQUEST);
    }
}