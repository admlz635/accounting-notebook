package com.agileengine.accountingnote.demo.exception;

public class AccountingNotebookException extends RuntimeException {

    public AccountingNotebookException(String description) {
        super(description);

    }
}
