import React from 'react';
import './App.css';
import AccountHistory from './AccountHistory'

const App: React.FC = () => {
  return (
    <div className="App">
      <header className="h1">
        <p>
          AgileEngine - Accounting Notebook
        </p>
      </header>
      <AccountHistory></AccountHistory>
    </div>
  );
}

export default App;
