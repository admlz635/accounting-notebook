import React from 'react';
import { Button, Collapse, ListGroup, ListGroupItem, Row, Col } from 'reactstrap';


interface Props {
}

interface State {
    transactionList: Transaction[],
    activeItem?: number
}

interface Transaction {
    id?: string,
    type?: Type,
    amount?: number,
    effectiveDate?: string,
}

enum Type {
    CREDIT = 'credit',
    DEBIT = 'debit'
}

export default class AccountHistory extends React.Component<Props, State> {

    componentDidMount() {
        this.fetchData();
    }
    // simple fetch function
    fetchData() {
        fetch('http://localhost:8080/api/transactions').then(response => {
            return response.json();
        }).then(data => this.setState({ transactionList: data }));
    }


    showMore(index: number) {
        if (this.state.activeItem === index) {
            this.setState({ activeItem: undefined });
            return;
        }
        this.setState({ activeItem: index });
    }
    render() {
        return (
            <div>
            <h2>Account History</h2>
            <ListGroup>
                {
                    this.state ? this.state.transactionList.map((trx, i) => (
                        <ListGroupItem key={i}>
                            <dl>
                                <Row style={{ backgroundColor: trx.type === Type.DEBIT ? '#e86f6f' : '#9bf298' }}>
                                    <Col xs="4"><dt>Type</dt><dd>{trx.type}</dd></Col>
                                    <Col xs="4"><dt>Amount</dt><dd> {trx.amount}</dd></Col>
                                    <Col xs="4"><dd /><Button color="primary" onClick={this.showMore.bind(this, i)} >+</Button></Col>
                                </Row>
                                <Collapse isOpen={this.state.activeItem === i} toggler="#toggler">
                                    <Row>
                                        <Col xs="4"><dt>Id</dt><dd> {trx.id}</dd></Col>
                                        <Col xs="4"><dt>EffectiveDate</dt><dd> {trx.effectiveDate}</dd></Col>
                                    </Row>
                                </Collapse>
                            </dl>

                        </ListGroupItem>
                    ))
                        :
                        null}
            </ListGroup>
            </div>
        );
    }
}
